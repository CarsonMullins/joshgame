package carsonmullins.net.joshgame;

/**
 * Created by carso on 3/28/2018.
 */

public class World {

    private int width, height;

    public World(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
