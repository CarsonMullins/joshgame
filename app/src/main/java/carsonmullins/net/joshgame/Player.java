package carsonmullins.net.joshgame;

/**
 * Created by carso on 3/28/2018.
 */

public class Player {

    private String name;
    private int x, y;

    public Player(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public int getYPos() {
        return y;
    }

    public int getXPos() {
        return x;
    }

    public String getName() {
        return name;
    }

    public void moveUp() {
        y++;
    }

    public void moveDown() {
        y--;
    }

    public void moveLeft() {
        x--;
    }

    public void moveRight() {
        x++;
    }

    @Override
    public String toString() {
        return name.toUpperCase()+"\n["+x+", "+y+"]";
    }

}
