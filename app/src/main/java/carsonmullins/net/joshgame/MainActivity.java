package carsonmullins.net.joshgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private Button upBtn, downBtn, leftBtn, rightBtn, inventoryBtn, statsBtn;
    private Player player;
    private World world;
    private Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ASSIGNMENTS

        textView = findViewById(R.id.textView);
        upBtn = findViewById(R.id.upBtn);
        downBtn = findViewById(R.id.downBtn);
        leftBtn = findViewById(R.id.leftBtn);
        rightBtn = findViewById(R.id.rightBtn);
        inventoryBtn = findViewById(R.id.inventoryBtn);
        statsBtn = findViewById(R.id.statsBtn);
        player = new Player("Shannon",10,10);
        world = new World(21,21);



        // LISTENERS

        upBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                moveUp();
            }
        });

        downBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                moveDown();
            }
        });

        leftBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                moveLeft();
            }
        });

        rightBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                moveRight();
            }
        });

        inventoryBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openInventory();
            }
        });

        statsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openStats();
            }
        });

        if(savedInstanceState != null) {
            textView.setText(savedInstanceState.getString("textView"));
        }else {
            textView.setText(player.getName()+"'s Game");
        }

    }

    public void moveUp() {
        if(player.getYPos() < world.getHeight()-1) {
            player.moveUp();
        }else {
            makeToast("You cannot move further up!");
        }
        textView.setText(player.toString());
    }

    public void moveDown() {
        if(player.getYPos() > 0) {
            player.moveDown();
        }else {
            makeToast("You cannot move further down!");
        }
        textView.setText(player.toString());
    }

    public void moveLeft() {
        if(player.getXPos() > 0) {
            player.moveLeft();
        }else {
            makeToast("You cannot move further left!");
        }
        textView.setText(player.toString());
    }

    public void moveRight() {
        if(player.getXPos() < world.getWidth()-1) {
            player.moveRight();
        }else {
            makeToast("You cannot move further right!");
        }
        textView.setText(player.toString());
    }

    public void openInventory() {
        Intent intent = new Intent(this,InventoryActivity.class);
        startActivity(intent);
    }

    public void openStats() {
        Intent intent = new Intent(this, StatsActivity.class);
        startActivity(intent);
    }

    public void makeToast(String str) {
        if(toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0,240);
        toast.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("textView",textView.getText().toString());
    }
}
